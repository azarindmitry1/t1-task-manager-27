package ru.t1.azarin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @NotNull
    public final static String NAME = "task-remove-by-id";

    @NotNull
    public final static String DESCRIPTION = "Remove task by id.";

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER TASK ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        serviceLocator.getTaskService().removeById(userId, id);
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
