package ru.t1.azarin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.azarin.tm.model.Project;
import ru.t1.azarin.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @NotNull
    public final static String NAME = "project-remove-by-id";

    @NotNull
    public final static String DESCRIPTION = "Remove project by id.";

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        @NotNull final Project project = serviceLocator.getProjectService().findOneById(userId, id);
        serviceLocator.getProjectTaskService().removeProjectById(userId, project.getId());
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
