package ru.t1.azarin.tm.exception.user;

public class UserIsLockedException extends AbstractUserException {

    public UserIsLockedException() {
        super("Error! User is locked...");
    }

}
